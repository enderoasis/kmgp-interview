<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Services\TreeDisplayService as TreeDisplay;

class TreeDisplayService 
{	

	public function make($data)
	{
		$itemsByReference = array();
		
		foreach($data as $key => &$item) 
		{
		   $itemsByReference[$item['id']] = &$item;
		   $itemsByReference[$item['id']]['children'] = array();
		}

		foreach($data as $key => &$item)
		{
		   if($item['parent_id'] && isset($itemsByReference[$item['parent_id']]))
		   {
		      $itemsByReference [$item['parent_id']]['children'][] = &$item;
		   }
		}

		foreach($data as $key => &$item) 
		{
		   if($item['parent_id'] && isset($itemsByReference[$item['parent_id']]))
		   {
		      unset($data[$key]);
		   }
		}	
	    
	    return response()->json($data, 200);
	}
}