<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TreeDisplayService as TreeDisplay;

class TreeController extends Controller
{
	public function __construct(TreeDisplay $treeDisplay)
	{
		$this->treeDisplay = $treeDisplay;
	}

    public function processArray(TreeDisplay $data)
    {
    	$data = request()->all();
     	return $this->treeDisplay->make($data);
    }
}
