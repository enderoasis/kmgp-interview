## Тестовое задание KMGP

Напишите приложение, где входом через GET будет отправляться Array(объект может быть любого размера)  в формате json с полями id, parrent_id, name <br>

по роутингу api/tree <br>

на выходе должен выйти объект в виде дерева.

# Инструкции

Для настройки проекта запустить команду: <br>
php artisan key:generate <br>
php artisan serve <br>
В корне директории есть файл для импорта в Postman для тестирования в ней заготовка запроса.

# Решение

Задействованы Контроллер, Сервис. Модель из-за ненадобности не использована, так как по api/tree мы принимаем на вход массив из json объектов. <br>
Обрабатываем и выдаем структурированный json ввиде дерева,где явно видны родители и их дочерние элементы.


